import pygame
import sys
import time
import random

# Variables
screen_width = 1920
screen_height = 1080

class Player(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load('assets/graphics/player/player.png').convert_alpha()
        self.image = pygame.transform.scale_by(self.image, 10)
        self.rect = self.image.get_rect(center = (screen_width // 2,screen_height // 2))
        self.direction = pygame.math.Vector2()
        self.speed = 5

    def player_input(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_w]:
            self.direction.y = -1
        if keys[pygame.K_s]:
            self.direction.y = 1
        if keys[pygame.K_w] and keys[pygame.K_s]:
            self.direction.y = 0
        if not keys[pygame.K_w] and not keys[pygame.K_s]:
            self.direction.y = 0

        if keys[pygame.K_a]:
            self.direction.x = -1
        if keys[pygame.K_d]:
            self.direction.x = 1
        if keys[pygame.K_a] and keys[pygame.K_d]:
            self.direction.x = 0
        if not keys[pygame.K_a] and not keys[pygame.K_d]:
            self.direction.x = 0

    def update(self):
        self.player_input()
        self.rect.center += (self.direction * self.speed)

class Background(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load('assets/graphics/background.png').convert_alpha()
        self.image = pygame.transform.scale_by(self.image, 10)
        self.rect = self.image.get_rect(topleft = (0,0))

class Camera(pygame.sprite.Group):
    def __init__(self):
        super().__init__()
        self.display_surface = pygame.display.get_surface()

        # Camera offset
        self.offset = pygame.math.Vector2()
        self.half_w = self.display_surface.get_size()[0] // 2
        self.half_h = self.display_surface.get_size()[1] // 2

    def center_target_camera(self,target):
        self.offset.x = player.rect.centerx - self.half_w - player.rect.width // 2
        self.offset.y = player.rect.centery - self.half_h - player.rect.height // 21

    def custom_draw(self,target):
        self.center_target_camera(target)

        # ground
        self.ground_offset = background.rect.topleft - self.offset
        print(self.ground_offset)
        print(background.rect.topleft)
        print(player.rect.center)
        print(player.direction)
        self.display_surface.blit(background.image,self.ground_offset)

        # # active elements
        # for sprite in sorted(self.sprites(),key = lambda sprite: sprite.rect.centery):
        #     offset_pos = sprite.rect.topleft + self.offset
        #     self.display_surface.blit(sprite.image,offset_pos)

pygame.init()
screen = pygame.display.set_mode((screen_width,screen_height))
clock = pygame.time.Clock()

# # Rectangles
player = Player()
camera = Camera()
background = Background()

player_group = pygame.sprite.GroupSingle()
player_group.add(player)

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit()

    player_group.update()

    camera.custom_draw(player)
    player_group.draw(screen)
    
    pygame.display.update()
    clock.tick(60)