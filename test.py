import pygame
import sys

# Variables
screen_width = 1920
screen_height = 1080

class Player(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load('assets/graphics/player/player.png').convert_alpha()
        self.image = pygame.transform.scale(self.image, (50, 50))
        self.rect = self.image.get_rect(center=(screen_width // 2, screen_height // 2))
        self.direction = pygame.math.Vector2(0, 0)
        self.speed = 8

    def player_input(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_w]:
            self.direction.y = -1
        elif keys[pygame.K_s]:
            self.direction.y = 1
        else:
            self.direction.y = 0

        if keys[pygame.K_a]:
            self.direction.x = -1
        elif keys[pygame.K_d]:
            self.direction.x = 1
        else:
            self.direction.x = 0

    def update(self):
        self.player_input()
        self.rect.center += (self.direction * self.speed)

class Background(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load('assets/graphics/background.png').convert_alpha()
        self.image = pygame.transform.scale(self.image, (screen_width, screen_height))
        self.rect = self.image.get_rect(topleft=(0, 0))

pygame.init()
screen = pygame.display.set_mode((screen_width, screen_height))
clock = pygame.time.Clock()

player = Player()
background = Background()
player_group = pygame.sprite.Group()
player_group.add(player)

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()

    player.update()

    # Clear the screen
    screen.fill((0, 0, 0))

    # Center the player on the screen
    player_rect = player.image.get_rect(center=(screen_width // 2, screen_height // 2))

    # Draw the background and player
    screen.blit(background.image, background.rect)
    screen.blit(player.image, player_rect)

    pygame.display.update()
    clock.tick(60)